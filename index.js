// Bài 1: Sắp xếp số theo thứ tự tăng dần

function baiTap1(){
    var num1Value = document.getElementById("num1").value*1;
    var num2Value = document.getElementById("num2").value*1;
    var num3Value = document.getElementById("num3").value*1;

    var maxNum = Math.max(num1Value,num2Value,num3Value);
    var minNum = Math.min(num1Value,num2Value,num3Value);


    if(num1Value == num2Value && num1Value == num3Value){
        document.getElementById("kq1").innerHTML = `${num1Value} = ${num2Value} = ${num3Value}`;
    }
    else if(num1Value == num2Value){
        if(num1Value>num3Value){
            document.getElementById("kq1").innerHTML = `${num3Value} < ${num1Value} = ${num2Value}`;
        }
        else{
            document.getElementById("kq1").innerHTML = `${num1Value} = ${num2Value} < ${num3Value}`;
        }
    }
    else if(num1Value == num3Value){
        if(num1Value>num2Value){
            document.getElementById("kq1").innerHTML = `${num2Value} < ${num1Value} = ${num3Value}`;
        }
        else{
            document.getElementById("kq1").innerHTML = `${num1Value} = ${num3Value} < ${num2Value}`;
        }
    }
    else if(num2Value == num3Value){
        if(num2Value>num1Value){
            document.getElementById("kq1").innerHTML = `${num1Value} < ${num2Value} = ${num3Value}`;
        }
        else{
            document.getElementById("kq1").innerHTML = `${num2Value} = ${num3Value} < ${num1Value}`;
        }
    }
    else{
        if(num1Value > minNum && num1Value < maxNum){
            document.getElementById("kq1").innerHTML = `${minNum} < ${num1Value} < ${maxNum}`;
        }
        else if(num2Value > minNum && num2Value < maxNum){
            document.getElementById("kq1").innerHTML = `${minNum} < ${num2Value} < ${maxNum}`;
        }
        else{
            document.getElementById("kq1").innerHTML = `${minNum} < ${num3Value} < ${maxNum}`;
        }
    }
}

// Bài 2: Gửi lời chào

function baiTap2(){
    var selectedValue = document.getElementById("txt-name").value;
    var result = null;

    if(selectedValue == "Chọn thành viên"){
        result = "Xin chào Người lạ ơi";
    }
    else if(selectedValue == "Bố"){
        result = "Xin chào bố"
    }
    else if(selectedValue == "Mẹ"){
        result = "Xin chào mẹ"
    }
    else if(selectedValue == "Anh trai"){
        result = "Xin chào anh trai"
    }
    else if(selectedValue == "Em gái"){
        result = "Xin chào em gái"
    }

    document.getElementById("baiTap2").innerHTML = result;
}

// Bài 3: Đếm số chẵn lẻ

function baiTap3(){
    var num1Value = document.getElementById("num1-b3").value*1;
    var num2Value = document.getElementById("num2-b3").value*1;
    var num3Value = document.getElementById("num3-b3").value*1;

    var count = 0;
    if(num1Value%2 == 0){
        count++;
        if(num2Value%2 == 0){
            count++;
            if(num3Value%2 == 0){
                count++;
            }
        }
        else if(num3Value%2 == 0){
            count++;
        }
    }
    else if(num2Value%2 == 0){
        count++;
        if(num3Value%2 == 0){
            count++
        }
    }
    else if(num3Value%2 == 0){
        count++;
    }
    else{
        count;
    }
    document.getElementById("bt3").innerHTML = `Có ${count} số chẵn và ${3-count} số lẻ `;
}

// Bài 4: Xác định loại tam giác

function baiTap4(){

    var edge1Value = document.getElementById("edge1").value*1;
    var edge2Value = document.getElementById("edge2").value*1;
    var edge3Value = document.getElementById("edge3").value*1;

    var maxEdge = Math.max(edge1Value,edge2Value,edge3Value);
    var minEdge = Math.min(edge1Value,edge2Value,edge3Value);
    var midEdge = null;

    var result = null;

    if(edge1Value > minEdge & edge1Value < maxEdge){
        midEdge = edge1Value;
    }
    else if(edge2Value > minEdge & edge2Value < maxEdge){
        midEdge = edge2Value;
    }
    else{
        midEdge = edge3Value;
    }


    if(edge1Value == edge2Value && edge1Value == edge3Value){
        result = "Tam giác đều";
    }
    else if(edge1Value == edge2Value || edge1Value == edge3Value || edge2Value == edge3Value){
        result = "Tam giác cân";
    }
    else if(maxEdge == Math.sqrt((minEdge*minEdge + midEdge*midEdge))){
        result = "Tam giác vuông";
    }
    else{
        result = "Tam giác khác";
    }

    document.getElementById("bt4").innerHTML = result;

}